using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Wizcorp.Utils.Logger;

public class SimpleDemo : MonoBehaviour {

	private IScanner BarcodeScanner;
	public Text TextHeader;
	public RawImage Image;
	public AudioSource Audio;

	Texture2D texture2D = null;
	public UnityEngine.UI.RawImage rawImage;
	public Material init_material;
	public Material project_material;
	public UnityEngine.UI.Text textinfo;


	public UnityEngine.UI.Button fullscreen_button;
	public UnityEngine.UI.Button exit_fullscreen_button;

	public UnityEngine.UI.Button closeButton;

	int reps = 0;

	// Disable Screen Rotation on that screen
	void Awake()
	{
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
	}

	void Start () {

		rawImage.material = init_material;
		closeButton.gameObject.SetActive(false);
		fullscreen_button.gameObject.SetActive(false);
		exit_fullscreen_button.gameObject.SetActive(false);
		//scanRegion.gameObject.SetActive(true);


		// Create a basic scanner
		BarcodeScanner = new Scanner();
		BarcodeScanner.Camera.Play();

		// Display the camera texture through a RawImage
		BarcodeScanner.OnReady += (sender, arg) => {
			// Set Orientation & Texture
			Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
			Image.transform.localScale = BarcodeScanner.Camera.GetScale();
			Image.texture = BarcodeScanner.Camera.Texture;

			// Keep Image Aspect Ratio
			var rect = Image.GetComponent<RectTransform>();
			var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
			rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
		};

		// Track status of the scanner
		BarcodeScanner.StatusChanged += (sender, arg) => {
			TextHeader.text = "Status: " + BarcodeScanner.Status;
		};
	}

	/// <summary>
	/// The Update method from unity need to be propagated to the scanner
	/// </summary>
	void Update()
	{
		if (BarcodeScanner == null)
		{
			return;
		}
		BarcodeScanner.Update();
	}

	#region UI Buttons

	public void ClickStart()
	{
		if (BarcodeScanner == null)
		{
			Log.Warning("No valid camera - Click Start");
			return;
		}

		// Start Scanning
		BarcodeScanner.Scan((barCodeType, barCodeValue) => {
			BarcodeScanner.Stop();
			TextHeader.text = "Found: " + barCodeType + " / " + barCodeValue;


			//my code here
			closeButton.gameObject.SetActive(true);
			fullscreen_button.gameObject.SetActive(true);
			exit_fullscreen_button.gameObject.SetActive(true);
			//scanRegion.gameObject.SetActive(false);

			reps++;
			Debug.Log("repetition : " + reps);

			textinfo.text = barCodeValue;

			Debug.Log("this is the link " + barCodeValue);

			//WWW contentLink = new WWW(barCodeValue);

			//https://docs.unity3d.com/ScriptReference/Networking.UnityWebRequestTexture.GetTexture.html

			// QRCode detected.
			//Application.OpenURL(data.Text);      // our function to call and pass url as text

			//my work here/
			textinfo.text = barCodeValue;
			/*
			WWW contentLink = new WWW(barCodeValue);

			while (!contentLink.isDone)
			{
				//yield return contentLink;
			}
			*/


			//UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(barCodeValue);

			/*while (!uwr.isDone)
			{
				//waiting
			}*/

			/*
			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.Log(uwr.error);
			}
			else
			{
				// Get downloaded asset bundle
				texture2D = DownloadHandlerTexture.GetContent(uwr);
			}
			*/

			Debug.Log(barCodeValue);
			

			StartCoroutine(GetTexture(barCodeValue));

			//rawImage.material = project_material;
			//texture2D = contentLink.texture;

			while (texture2D != null)
			{
				continue;
			}

			/*
			Debug.Log("lets see what happens : " + texture2D == null);
			//Debug.Log("these are the values " + texture2D.width +" :: " + texture2D.height);

			//rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
			rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
			rawImage.material = project_material;
			rawImage.texture = texture2D;
			*/
			// Feedback
			Audio.Play();

			#if UNITY_ANDROID || UNITY_IOS
			Handheld.Vibrate();
			#endif


		});
	}


	IEnumerator GetTexture(String source)
	{
		using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(source))
		{
			//yield return uwr.SendWebRequest();
			//yield return new WaitUntil(() => texture2D != null);
			uwr.downloadHandler = new DownloadHandlerTexture();
			yield return uwr.SendWebRequest();

			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.Log(uwr.error);
			}
			else
			{
				// Get downloaded asset bundle
				//AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
				texture2D = DownloadHandlerTexture.GetContent(uwr);

				Debug.Log(uwr.isDone);
				Debug.Log(uwr);
				Debug.Log(texture2D.dimension);

				Debug.Log("lets see what happens : " + texture2D == null);
				Debug.Log("these are the values " + texture2D.width +" :: " + texture2D.height);

				//rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
				rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
				rawImage.material = project_material;
				rawImage.texture = texture2D;
			}
		}
	}


public void ClickStop()
	{
		if (BarcodeScanner == null)
		{
			Log.Warning("No valid camera - Click Stop");
			return;
		}

		// Stop Scanning
		BarcodeScanner.Stop();
	}

	public void ClickBack()
	{
		// Try to stop the camera before loading another scene
		StartCoroutine(StopCamera(() => {
			SceneManager.LoadScene("Boot");
		}));
	}

	/// <summary>
	/// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
	/// Trying to stop the camera in OnDestroy provoke random crash on Android
	/// </summary>
	/// <param name="callback"></param>
	/// <returns></returns>
	public IEnumerator StopCamera(Action callback)
	{
		// Stop Scanning
		Image = null;
		BarcodeScanner.Destroy();
		BarcodeScanner = null;

		// Wait a bit
		yield return new WaitForSeconds(0.1f);

		callback.Invoke();
	}



	public void close()
	{
		//rawImage.material = init_material;
		closeButton.gameObject.SetActive(false);
		fullscreen_button.gameObject.SetActive(false);
		exit_fullscreen_button.gameObject.SetActive(false);
		rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 1000);
		//rawImage.uvRect = new Rect(0, 0, 100, 100);
		rawImage.material = init_material;
		//scanRegion.gameObject.SetActive(true);
	}

	#endregion
}
