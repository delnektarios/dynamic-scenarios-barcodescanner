using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ContinuousDemo : MonoBehaviour {

	private IScanner BarcodeScanner;
	public Text TextHeader;
	public RawImage Image;
	public AudioSource Audio;
	private float RestartTime;

	Texture2D texture2D;
	public UnityEngine.UI.RawImage rawImage;
	public Material init_material;
	public Material project_material;
	public UnityEngine.UI.Text textinfo;


	public UnityEngine.UI.Button fullscreen_button;
	public UnityEngine.UI.Button exit_fullscreen_button;

	public UnityEngine.UI.Button closeButton;


	//public UnityEngine.UI.Button scanRegion;

	// Disable Screen Rotation on that screen
	void Awake()
	{
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
	}

	void Start () {

		rawImage.material = init_material;
		closeButton.gameObject.SetActive(false);
		fullscreen_button.gameObject.SetActive(false);
		exit_fullscreen_button.gameObject.SetActive(false);
		//scanRegion.gameObject.SetActive(true);

		// Create a basic scanner
		BarcodeScanner = new Scanner();
		BarcodeScanner.Camera.Play();

		// Display the camera texture through a RawImage
		BarcodeScanner.OnReady += (sender, arg) => {
			// Set Orientation & Texture
			Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
			Image.transform.localScale = BarcodeScanner.Camera.GetScale();
			Image.texture = BarcodeScanner.Camera.Texture;

			// Keep Image Aspect Ratio
			var rect = Image.GetComponent<RectTransform>();
			var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
			rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);

			RestartTime = Time.realtimeSinceStartup;
		};
	}

	/// <summary>
	/// Start a scan and wait for the callback (wait 1s after a scan success to avoid scanning multiple time the same element)
	/// </summary>
	private void StartScanner()
	{

		BarcodeScanner.Scan((barCodeType, barCodeValue) => {
			BarcodeScanner.Stop();
			if (TextHeader.text.Length > 10)
			{
				TextHeader.text = "";
			}
			TextHeader.text += "Found: " + barCodeType + " / " + barCodeValue + "\n";
			RestartTime += Time.realtimeSinceStartup + 1f;

			StartCoroutine(GetTexture(barCodeValue));

			// Feedback
			Audio.Play();

			#if UNITY_ANDROID || UNITY_IOS
			Handheld.Vibrate();
			#endif
		});



		//WWW contentLink = new WWW(barCodeValue);

		//https://docs.unity3d.com/ScriptReference/Networking.UnityWebRequestTexture.GetTexture.html

		// QRCode detected.
		//Application.OpenURL(data.Text);      // our function to call and pass url as text

		//my work here



		//------------
	}

	IEnumerator GetTexture(String source)
	{
		using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(source))
		{
			//yield return uwr.SendWebRequest();
			//yield return new WaitUntil(() => texture2D != null);
			uwr.downloadHandler = new DownloadHandlerTexture();
			yield return uwr.SendWebRequest();

			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.Log(uwr.error);
			}
			else
			{
				// Get downloaded asset bundle
				//AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
				texture2D = DownloadHandlerTexture.GetContent(uwr);

				Debug.Log(uwr.isDone);
				Debug.Log(uwr);
				Debug.Log(texture2D.dimension);

				Debug.Log("lets see what happens : " + texture2D == null);
				Debug.Log("these are the values " + texture2D.width + " :: " + texture2D.height);

				//rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
				rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
				rawImage.material = project_material;
				rawImage.texture = texture2D;
			}
		}
	}

	/*
	IEnumerator GetTexture(string barCodeValue)
	{
		using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(barCodeValue))
		{
			yield return uwr.SendWebRequest();

			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.Log(uwr.error);
			}
			else
			{
				// Get downloaded asset bundle
				var texture = DownloadHandlerTexture.GetContent(uwr);
			}
		}
	}
	*/

/*
	IEnumerator downloadImage(string barCodeUrl)
	{
		string url = barCodeUrl;

		UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);

		DownloadHandler handle = www.downloadHandler;

		//Send Request and wait
		yield return www.SendWebRequest();

		if (www.isHttpError || www.isNetworkError)
		{
			UnityEngine.Debug.Log("Error while Receiving: " + www.error);
		}
		else
		{
			UnityEngine.Debug.Log("Success");

			//Load Image
			Texture2D texture2d = DownloadHandlerTexture.GetContent(www);

			Sprite sprite = null;
			sprite = Sprite.Create(texture2d, new Rect(0, 0, texture2d.width, texture2d.height), Vector2.zero);

			if (sprite != null)
			{
				texture2D = sprite;
			}
		}
	}
	*/

		
	/// <summary>
	/// The Update method from unity need to be propagated
	/// </summary>
	void Update()
	{

		if (BarcodeScanner != null)
		{
			BarcodeScanner.Update();
		}

		// Check if the Scanner need to be started or restarted
		if (RestartTime != 0 && RestartTime < Time.realtimeSinceStartup)
		{
			StartScanner();
			RestartTime = 0;
		}
	}
	

	#region UI Buttons

	public void ClickBack()
	{
		// Try to stop the camera before loading another scene
		StartCoroutine(StopCamera(() => {
			SceneManager.LoadScene("Boot");
		}));
	}

	/// <summary>
	/// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
	/// Trying to stop the camera in OnDestroy provoke random crash on Android
	/// </summary>
	/// <param name="callback"></param>
	/// <returns></returns>
	public IEnumerator StopCamera(Action callback)
	{
		// Stop Scanning
		Image = null;
		BarcodeScanner.Destroy();
		BarcodeScanner = null;

		// Wait a bit
		yield return new WaitForSeconds(0.1f);

		callback.Invoke();
	}

	#endregion


	public void close()
	{
		//rawImage.material = init_material;
		closeButton.gameObject.SetActive(false);
		fullscreen_button.gameObject.SetActive(false);
		exit_fullscreen_button.gameObject.SetActive(false);
		rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 1000);
		//rawImage.uvRect = new Rect(0, 0, 100, 100);
		rawImage.material = init_material;
		//scanRegion.gameObject.SetActive(true);
	}


}
